import pydicom
import os
import requests
import json
import pandas

def read_config(file_path='config.json'):
    with open(file_path, 'r') as config_file:
        config = json.load(config_file)
    return config['xnat']

def stage_dicom_ecgs():
    #load config
    xnat_config = read_config()

    project_id = xnat_config['project_id']
    ecg_folder_path = xnat_config['ecg_folder_path']
    path_to_save_metadata_csv = xnat_config['path_to_metadata_csv']

    # throw an exception when folder is empty or not findable
    if not xnat_config['ecg_folder_path'] or not os.path.exists(xnat_config['ecg_folder_path']):
        raise ValueError("The ECG folder is empty or not findable.")
    metadata_of_ecgs = get_metadata(ecg_folder_path, project_id)
    metadata_of_ecgs.to_csv(path_to_save_metadata_csv, index=False)

def get_metadata(ecg_folder_path, project_id)-> pandas.DataFrame:
    ecg_metadata = pandas.DataFrame(columns=["project_id", "series_instance_id", "study_instance_id", "patient_id",
                                             "series_description", "filepath"])
    # Iterate through DICOM files in the specified folder
    for root, dirs, files in os.walk(ecg_folder_path):
        for file_name in files:
            dicom_path = os.path.join(root, file_name)

            # Read DICOM file using pydicom
            dicom_data = pydicom.dcmread(dicom_path)

            # Extract relevant attributes for mapping
            series_instance_uid = dicom_data.SeriesInstanceUID
            study_instance_uid = dicom_data.StudyInstanceUID
            patient_id = dicom_data.PatientID
            series_description = dicom_data.SeriesDescription

            new_row = {'project_id': project_id, 'series_instance_id': series_instance_uid,
                       'study_instance_id': study_instance_uid, 'patient_id': patient_id,
                       'series_description': series_description, "filepath": dicom_path}
            ecg_metadata.loc[len(ecg_metadata)]  = new_row
    return ecg_metadata

def authenticate_xnat():
    xnat_config = read_config()
    base_url = xnat_config['base_url']
    username = xnat_config['username']
    password = xnat_config['password']

    # Create a session for authentication
    session = requests.Session()
    session.auth = (username, password)

    return base_url, session

def upload_dicom_ecg(row, base_url, session):
    project_id = row["project_id"]
    series_instance_id = row["series_instance_id"]
    study_instance_id = row["study_instance_id"]
    series_description = row["series_description"]
    patient_id = row["patient_id"]
    dicom_path = row["filepath"]
    file_name = os.path.basename(dicom_path)

    # Create XNAT REST API URLs
    subject_url = f"{base_url}/data/projects/{project_id}/subjects/{patient_id}"
    session_url = f"{base_url}/data/projects/{project_id}/subjects/{patient_id}/experiments/{study_instance_id}?xsiType=ecgSessionData"
    scan_url = f"{base_url}/data/projects/{project_id}/subjects/{patient_id}/experiments/{study_instance_id}/scans/{series_instance_id}?xsiType=ecgScanData"
    scan_type_url = f"{base_url}/data/projects/{project_id}/subjects/{patient_id}/experiments/{study_instance_id}/scans/{series_instance_id}?type={series_description}"
    scan_series_description_url = f"{base_url}/data/projects/{project_id}/subjects/{patient_id}/experiments/{study_instance_id}/scans/{series_instance_id}?series_description={series_description}"
    dicom_resource_url = f"{base_url}/data/projects/{project_id}/subjects/{patient_id}/experiments/{study_instance_id}/scans/{series_instance_id}/resources/DICOM"

    # Make XNAT REST API calls to create session, scan, and DICOM resource
    subject_response = session.put(subject_url)
    session_response = session.put(session_url)
    scan_response = session.put(scan_url)
    scan_type_response = session.put(scan_type_url)
    scan_series_description_response = session.put(scan_series_description_url)
    dicom_resource_response = session.put(dicom_resource_url, files={'file': open(dicom_path, 'rb')})

    # Upload DICOM file to XNAT
    dicom_file_url = f"{dicom_resource_url}/files/{file_name}?inbody=true"
    dicom_file_response = session.put(dicom_file_url, data=open(dicom_path, 'rb'))

    # Check response status for each API call
    print(f"Subject Response: {subject_response.status_code}")
    print(f"Session Response: {session_response.status_code}")
    print(f"Scan Response: {scan_response.status_code}")
    print(f"Scan type Response: {scan_type_response.status_code}")
    print(f"Scan series description Response: {scan_series_description_response.status_code}")
    print(f"DICOM Resource Response: {dicom_resource_response.status_code}")
    print(f"DICOM File Upload Response: {dicom_file_response.status_code}")

def handle_upload_ecgs():
    base_url, session = authenticate_xnat()
    xnat_config = read_config()
    metadata_csv_path = xnat_config['path_to_metadata_csv']

    # Read the CSV file
    ecg_metadata = pandas.read_csv(metadata_csv_path)

    # Display the current metadata
    print("Current Metadata:")
    print(ecg_metadata)

    # Ask the user if they want to make changes in a text editor
    user_input = input("Do you want to make changes to the metadata CSV file in a text editor? (y/n): ").lower()

    if user_input == 'y':
        # Open the CSV file in a text editor
        os.system(f"start {metadata_csv_path}")

        # Wait for the user to finish editing
        input("Press Enter when you have finished editing in the text editor.")

        # Read the CSV file again after potential changes
        ecg_metadata = pandas.read_csv(metadata_csv_path)

    # Upload the modified metadata
    ecg_metadata.apply(upload_dicom_ecg, axis=1, base_url=base_url, session=session)



